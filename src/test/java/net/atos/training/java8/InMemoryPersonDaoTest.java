package net.atos.training.java8;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.List;

public class InMemoryPersonDaoTest {
    private final InMemoryPersonDao subject = new InMemoryPersonDao();

    @Test
    public void should_return_both_jimmy_and_tammy_for_filter_mmy() throws Exception {
        // GIVEN
        String filter = "mmy";

        // WHEN
        List<Person> actual = subject.getPersons(filter);

        // THEN
        Assertions.assertThat(actual).containsOnly(
                new Person("Jimmy Johnston", "jjohnston6"),
                new Person("Tammy Clark", "tclarkd"),
                new Person("Jimmy Carpenter", "jcarpenteri")
        );
    }

    @Test
    public void should_return_both_jimmy_and_tammy_names_for_filter_mmy() throws Exception {
        // GIVEN
        String filter = "mmy";

        // WHEN
        List<String> actual = subject.getPersonsNames(filter);

        // THEN
        Assertions.assertThat(actual).containsOnly(
                "Jimmy Johnston",
                "Tammy Clark",
                "Jimmy Carpenter"
        );
    }

    @Test
    public void should_return_3_for_filter_mmy() throws Exception {
        // GIVEN
        String filter = "mmy";

        // WHEN
        long actual = subject.getNumberOfFilteredPersons(filter);

        // THEN
        Assertions.assertThat(actual).isEqualTo(3);
    }
}