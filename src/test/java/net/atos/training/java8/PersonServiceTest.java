package net.atos.training.java8;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.function.Consumer;

public class PersonServiceTest {
    private final PersonService subject = new PersonService();

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_if_alias_is_null() throws Exception {
        // GIVEN
        Person jean = Person.builder().name("Jean").build();

        // WHEN
        subject.getAlias(jean);

        // THEN
        // Throws the exception
    }

    @Test()
    public void should_return_alias_if_exists() throws Exception {
        // GIVEN
        Person jean = Person.builder().name("Jean").alias("De la Playa").build();

        // WHEN
        String actual = subject.getAlias(jean);

        // THEN
        Assertions.assertThat(actual).isEqualTo("De la Playa");
    }

    @Test
    public void should_not_be_called_if_no_alias() throws Exception {
        // GIVEN
        Person jean = Person.builder().name("Jean").build();
        Consumer<String> mockConsumer = Mockito.mock(Consumer.class);

        // WHEN
        subject.executeIfAliasPresent(jean, mockConsumer);

        // THEN
        Mockito.verifyZeroInteractions(mockConsumer);
    }

    @Test
    public void should_be_called_if_alias_present() throws Exception {
        // GIVEN
        Person jean = Person.builder().name("Jean").alias("De la Playa").build();
        Consumer<String> mockConsumer = Mockito.mock(Consumer.class);

        // WHEN
        subject.executeIfAliasPresent(jean, mockConsumer);

        // THEN
        Mockito.verify(mockConsumer).accept("De la Playa");
    }

    @Test
    public void should_return_marc_and_matthieu() throws Exception {
        // GIVEN

        // WHEN

        // THEN
    }

}