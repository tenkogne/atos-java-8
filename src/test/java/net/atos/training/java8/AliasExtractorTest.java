package net.atos.training.java8;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class AliasExtractorTest {
    private final AliasExtractor subject = new AliasExtractor();

    @Test
    public void should_return_alias_if_it_exists() throws Exception {
        // GIVEN
        Person person = Person.builder().name("Jean").alias("De la Playa").build();

        // WHEN
        String actual = subject.getAlias(person);

        // THEN
        Assertions.assertThat(actual).isEqualTo("De la Playa");
    }

    @Test
    public void should_return_null_if_alias_does_not_exist() throws Exception {
        // GIVEN
        Person person = Person.builder().name("Jean").build();

        // WHEN
        String actual = subject.getAlias(person);

        // THEN
        Assertions.assertThat(actual).isNullOrEmpty();
    }
}