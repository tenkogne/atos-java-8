package net.atos.training.java8;

import java.util.List;

public interface PersonDao {
    List<Person> getPersons(String nameFilter);

    List<String> getPersonsNames(String nameFilter);
}
