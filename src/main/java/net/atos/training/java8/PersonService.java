package net.atos.training.java8;

import java.util.List;
import java.util.function.Consumer;

public class PersonService {
    private final PersonDao personDao = new InMemoryPersonDao();
    private final AliasExtractor extractor = new AliasExtractor();

    /**
     * @param person
     * @return alias or else throw IllegalArgumentException if no alias for this person to avoid returning null
     */
    public String getAlias(Person person) {
        String alias = extractor.getAlias(person);
        if (alias == null) {
            throw new IllegalArgumentException("The provided person should have an alias");
        }
        return alias;
    }

    public void executeIfAliasPresent(Person person, Consumer<String> fnToExecuteOnAlias) {
        String alias = extractor.getAlias(person);
        if (alias != null) {
            fnToExecuteOnAlias.accept(alias);
        }
    }

    public List<Person> getPersons(String nameFilter) {
        return personDao.getPersons(nameFilter);
    }

    public List<String> getPersonsNames(String nameFilter) {
        return personDao.getPersonsNames(nameFilter);
    }
}
