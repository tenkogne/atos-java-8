package net.atos.training.java8;

public class AliasExtractor {
    public String getAlias(Person person) {
        if (person == null) {
            return null;
        }
        return person.getAlias();
    }
}
