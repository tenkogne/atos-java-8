package net.atos.training.java8;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Builder;

@Getter
@Builder
@ToString
@EqualsAndHashCode
public class Person {
    private String name;
    private String alias;
}
