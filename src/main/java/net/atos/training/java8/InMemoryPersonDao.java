package net.atos.training.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InMemoryPersonDao implements PersonDao {
    private static final List<Person> PERSONS = Arrays.asList(
            new Person("Chris Schmidt", "cschmidt0"),
            new Person("Ryan Robinson", "rrobinson1"),
            new Person("Shirley Payne", "spayne2"),
            new Person("Kevin Kelly", "kkelly3"),
            new Person("Phyllis Smith", "psmith4"),
            new Person("John Spencer", "jspencer5"),
            new Person("Jimmy Johnston", "jjohnston6"),
            new Person("Barbara Brooks", "bbrooks7"),
            new Person("Rachel Castillo", "rcastillo8"),
            new Person("Gerald Kelley", "gkelley9"),
            new Person("Sean Lawrence", "slawrencea"),
            new Person("Jack Hanson", "jhansonb"),
            new Person("Carolyn Gordon", "cgordonc"),
            new Person("Tammy Clark", "tclarkd"),
            new Person("Stephen Mills", "smillse"),
            new Person("Donna Moore", "dmooref"),
            new Person("Jessica Russell", "jrussellg"),
            new Person("Robert Ramos", "rramosh"),
            new Person("Jimmy Carpenter", "jcarpenteri")
    );

    public List<Person> getPersons(String nameFilter) {
        ArrayList<Person> filteredPersons = new ArrayList<>();
        for (Person person : PERSONS) {
            if (person.getName().toLowerCase().contains(nameFilter.toLowerCase())) {
                filteredPersons.add(person);
            }
        }
        return filteredPersons;
    }

    public List<String> getPersonsNames(String nameFilter) {
        ArrayList<String> filteredPersons = new ArrayList<>();
        for (Person person : PERSONS) {
            if (person.getName().toLowerCase().contains(nameFilter.toLowerCase())) {
                filteredPersons.add(person.getName());
            }
        }
        return filteredPersons;
    }

    public long getNumberOfFilteredPersons(String nameFilter) {
        int numberOfFilteredPersons = 0;
        for (Person person : PERSONS) {
            if (person.getName().toLowerCase().contains(nameFilter.toLowerCase())) {
                numberOfFilteredPersons++;
            }
        }
        return numberOfFilteredPersons;
    }
}
